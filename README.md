Un simulateur de budget pour un habitat partagé où les contributions dépendent des revenus des habitant·es.

## Utilisation

Survolez les valeurs en bleu, et déplacez le curseur à gauche et à droite pour modifier les valeurs.

## Fonctionnalités

- Interface textuelle interactive
- Mise à jour instantanée des données
- Partagez la simulation courante en envoyant l'URL

## Implémentation

Ce simulateur utilise Vue.js 2.0, ainsi que VueTangleKit.js, une port de la bibliothèque [TangleKit](http://worrydream.com/Tangle/) de Bret Victor en Vue.js.

Aucun compilateur ou transpileur n'est nécessaire.
