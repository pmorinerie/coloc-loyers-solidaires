//
//  VueTangleKit.js
//  A port of TangleKit using Vue.js components
//
//  Created by Bret Victor on 6/10/11.
//  Port by Pierre de La Morinerie on 11/06/2018.
//  (c) 2011 Bret Victor.  MIT open-source license.
//

//----------------------------------------------------------
//
//  tk-adjustable-number
//
//  Drag a number to adjust.
//
//  Attributes:  min (optional): minimum value
//               max (optional): maximum value
//               step (optional): granularity of adjustment (can be fractional)
//               scale (optional): how much the cursor should move to go to the next step (can be fractional)

Vue.component('tk-adjustable-number', {
  props: {
    value: { type: Number, required: true },
    min:   { type: Number, default: 1 },
    max:   { type: Number, default: 10 },
    step:  { type: Number, default: 1 },
    scale: { type: Number, default: 5 }
  },

  data: function () {
    return {
      valueAtMouseDown: 0,
      clientXAtMouseDown: 0,
      isHovering: false,
      isDragging: false,
      mouseBound: {
        onTouchMoveHandler: null,
        onPointerMoveHandler: null,
        onPointerUpHandler: null
      }
    }
  },

  computed: {
    isActive: function() {
      return this.isDragging || this.isHovering;
    },
    hasHoverClass: function() {
      return !this.isDragging && this.isActive;
    }
  },

  methods: {
    clamp: function(number, min, max) {
      return Math.min(Math.max(number, min), max);
    },

    updateRolloverEffects: function () {
      this.updateCursor();
      this.updateHelp();
    },

    updateCursor: function () {
      let body = document.body;
      if (this.isActive) { body.classList.add("TKCursorDragHorizontal"); }
      else { body.classList.remove("TKCursorDragHorizontal"); }
    },

    // Help
    updateHelp: function () {
      let size = this.$refs.root.getBoundingClientRect(),
          top = -size.height;
          left = Math.round(0.5 * (size.width - 25));
          display = this.isHovering ? "block" : "none";

      let helpElement = this.$refs.help;
      helpElement.style.left = left + 'px';
      helpElement.style.top = top + 'px';
      helpElement.style.display = display;
    },

    // Hover
    onPointerEnter: function() {
      this.isHovering = true;
      this.updateRolloverEffects();
    },

    onPointerLeave: function() {
      this.isHovering = false;
      this.updateRolloverEffects();
    },

    // Drag
    onPointerDown: function(pointerEvent) {
      this.valueAtMouseDown = this.value;
      this.clientXAtMouseDown = pointerEvent.clientX;
      this.isDragging = true;
      this.updateRolloverEffects();
      // Register mouse move events
      this.mouseBound = {
        onTouchMoveHandler: this.onTouchMove.bind(this),
        onPointerMoveHandler: this.onPointerMove.bind(this),
        onPointerUpHandler: this.onPointerUp.bind(this)
      }
      document.addEventListener('touchmove', this.mouseBound.onTouchMoveHandler, { passive: false });
      document.addEventListener('pointermove', this.mouseBound.onPointerMoveHandler);
      document.addEventListener('pointerup',   this.mouseBound.onPointerUpHandler);

      // Prevent the text from being selected
      pointerEvent.preventDefault();
    },

    onTouchMove: function(pointerEvent) {
      // Prevent the browser to cancel the pointer events after a while
      pointerEvent.preventDefault();
    },

    onPointerMove: function(pointerEvent) {
      let offsetX = pointerEvent.clientX - this.clientXAtMouseDown,
      value = this.valueAtMouseDown + offsetX / this.scale * this.step;
      newValue = Math.round(value / this.step) * this.step;
      this.$emit('input', this.clamp(newValue, this.min, this.max));
      this.updateHelp();

      pointerEvent.preventDefault();
    },

    onPointerUp: function(pointerEvent) {
      this.isDragging = false;
      if (this.mouseBound) {
        document.removeEventListener('touchmove', this.mouseBound.onTouchMoveHandler);
        document.removeEventListener('pointermove', this.mouseBound.onPointerMoveHandler);
        document.removeEventListener('pointerup',   this.mouseBound.onPointerUpHandler);
        this.mouseBound = null;
      }
      this.updateRolloverEffects();
      this.$refs.help.style.display = 'none';
    }
  },

  template: '<span ref="root" \
                   class="TKAdjustableNumber" \
                   v-bind:class="{ TKAdjustableNumberDown: isDragging, TKAdjustableNumberHover: hasHoverClass }" \
                   v-on:pointerenter="onPointerEnter" \
                   v-on:pointerleave="onPointerLeave" \
                   v-on:pointerdown="onPointerDown"> \
                <div ref="help" class="TKAdjustableNumberHelp" style="display: none">drag</div> \
                {{value}}<slot></slot> \
             </span>'
});


//----------------------------------------------------------
//
//  tk-inconspicuous-input
//
//  Click on the text to edit it in-place.
//
//  (Addition to the original TangleKit by kemenaran)

Vue.component('tk-inconspicuous-input', {
  props: {
    value: { type: String, required: true }
  },

  mounted: function() {
    // On the initial render, adjust the input width.
    this.adjustWidth(this.value);
  },

  updated: function() {
    // When an external change causes the value to be updated, adjust the input width.
    this.adjustWidth(this.value);
  },

  methods: {
    onInput: function(event) {
      let newValue = event.target.value;
      // When typing into the input causes the value to be updated, adjust the input width.
      this.adjustWidth(newValue);
      this.$emit('input', newValue);
    },

    adjustWidth: function(value) {
      this.$refs.meter.innerText = value;
      this.$refs.input.style.width = this.$refs.meter.getBoundingClientRect().width + 'px';
    }
  },

  template: '<span class="TKInconspicuousInput"><span ref="meter" class="meter"></span><input ref="input" v-bind:value="value" v-on:input="onInput"/></span>'
});
